<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_package', function (Blueprint $table) {
            $table->float('amount')->default(0);
            
            $table->integer('sale_id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->primary(['sale_id', 'package_id']);
            $table->foreign('sale_id')
                ->references('id')->on('sales');
            $table->foreign('package_id')
                ->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_package');
    }
}
