<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotePackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_package', function (Blueprint $table) {
            $table->float('amount')->default(0);
            
            $table->integer('quote_id')->unsigned();
            $table->integer('package_id')->unsigned();
            $table->primary(['quote_id', 'package_id']);
            $table->foreign('quote_id')
                ->references('id')->on('quotes');
            $table->foreign('package_id')
                ->references('id')->on('packages');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quote_package');
    }
}
