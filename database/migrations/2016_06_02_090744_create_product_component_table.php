<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductComponentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_component', function (Blueprint $table) {
            $table->float('amount')->default(0);
            
            $table->integer('product_id')->unsigned();
            $table->integer('component_id')->unsigned();
            $table->primary(['product_id', 'component_id']);
            $table->foreign('product_id')
                ->references('id')->on('products');
            $table->foreign('component_id')
                ->references('id')->on('components');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_component');
    }
}
