<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_product', function (Blueprint $table) {
            $table->float('amount')->default(0);
            
            $table->integer('package_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['package_id', 'product_id']);
            $table->foreign('package_id')
                ->references('id')->on('packages');
            $table->foreign('product_id')
                ->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('package_product');
    }
}
