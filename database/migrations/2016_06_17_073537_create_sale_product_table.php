<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_product', function (Blueprint $table) {
            $table->float('amount')->default(0);

            $table->integer('sale_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['sale_id', 'product_id']);
            $table->foreign('sale_id')
                ->references('id')->on('sales');
            $table->foreign('product_id')
                ->references('id')->on('products');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_product');
    }
}
