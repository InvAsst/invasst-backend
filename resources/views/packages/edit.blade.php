@extends('main')

@section('content')

    <h1>Editar Paquete</h1>

    {{ Form::model($package, ['method' => 'PUT', 'route' => ['package.update', $package->id]]) }}

    <ul>

        <li>
            {{ Form::label('code', 'Clave:') }}
            {{ Form::text('code') }}
        </li>

        <li>
            {{ Form::label('name', 'Nombre:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('description', 'Descripcion:') }}
            {{ Form::text('description') }}
        </li>

        <li>
            {{ Form::label('image', 'Imagen:') }}
            {{ Form::text('image') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
            {{ link_to_route('package.show', 'Cancel', $package->id, array('class' => 'btn')) }}

        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop