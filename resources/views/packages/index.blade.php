@extends('main')

@section('content')

    <h1>Paquetes</h1>

    <p>{{ link_to_route('package.create', 'Agregar nuevo paquete') }}</p>
    @if ($packages->count())
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Clave</th>
                <th>Nombre</th>
                <th>Descripcion</th>
                <th>Imagen</th>
            </tr>
            </thead>

            <tbody>

            @foreach ($packages as $package)
                <tr>
                    <td>{{ $package['code'] }}</td>
                    <td>{{ $package['name'] }}</td>
                    <td>{{ $package['description'] }}</td>
                    <td>{{ $package['image'] }}</td>
                    <td>{{ link_to_route('package.edit', 'Edit', [$package['id']], array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['package.destroy', $package['id']]]) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
    @else
        There are no users
    @endif

@stop