@section('sidebar')

    <div class="nav">
        <!-- Sidebar Menu -->
        <ul class="nav nav-tabs">
            <!-- Optionally, you can add icons to the links -->
            <li>{!! link_to_route("client.index", "Clientes") !!}</li>
            <li>{!! link_to_route("component.index", "Componentes") !!}</li>
            <li>{!! link_to_route("product.index", "Productos") !!}</li>
            <li>{!! link_to_route("package.index", "Paquetes") !!}</li>
        </ul><!-- /.sidebar-menu -->

    </div><!-- /.main-sidebar -->

@show