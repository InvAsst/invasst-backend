@extends('main')

@section('content')

    <h1>Components</h1>

    <p>{{ link_to_route('component.create', 'Add new component') }}</p>
    @if ($components->count())
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Clave</th>
                <th>Descripcion</th>
                <th>Costo</th>
                <th>Inventario</th>
                <th>Imagen</th>
            </tr>
            </thead>

            <tbody>

            @foreach ($components as $component)
                <tr>
                    <td>{{ $component['code'] }}</td>
                    <td>{{ $component['description'] }}</td>
                    <td>{{ $component['price'] }}</td>
                    <td>{{ $component['stock'] }}</td>
                    <td>{{ $component['image'] }}</td>
                    <td>{{ link_to_route('component.edit', 'Edit', [$component['id']], array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['component.destroy', $component['id']]]) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
    @else
        There are no users
    @endif

@stop