@extends('main')

@section('content')

    <h1>Edit Component</h1>

    {{ Form::model($component, ['method' => 'PUT', 'route' => ['component.update', $component->id]]) }}

    <ul>

        <li>
            {{ Form::label('code', 'Clave:') }}
            {{ Form::text('code') }}
        </li>

        <li>
            {{ Form::label('description', 'Descripcion:') }}
            {{ Form::text('description') }}
        </li>

        <li>
            {{ Form::label('cost', 'Costo:') }}
            {{ Form::text('cost') }}
        </li>

        <li>
            {{ Form::label('stock', 'Inventario:') }}
            {{ Form::text('stock') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
            {{ link_to_route('component.show', 'Cancel', $component->id, array('class' => 'btn')) }}

        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop