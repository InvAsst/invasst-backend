@extends('main')

@section('content')

    <h1>Create Product</h1>

    {{ Form::open(array('route' => 'product.store')) }}
    <ul>

        <li>
            {{ Form::label('code', 'Clave:') }}
            {{ Form::text('code') }}
        </li>

        <li>
            {{ Form::label('description', 'Descripcion:') }}
            {{ Form::text('description') }}
        </li>

        <li>
            {{ Form::label('price', 'Precio:') }}
            {{ Form::text('price') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop