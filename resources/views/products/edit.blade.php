@extends('main')

@section('content')

    <h1>Edit Product</h1>

    {{ Form::model($product, ['method' => 'PUT', 'route' => ['product.update', $product->id]]) }}

    <ul>

        <li>
            {{ Form::label('code', 'Clave:') }}
            {{ Form::text('code') }}
        </li>

        <li>
            {{ Form::label('description', 'Descripcion:') }}
            {{ Form::text('description') }}
        </li>

        <li>
            {{ Form::label('price', 'Precio:') }}
            {{ Form::text('price') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
            {{ link_to_route('product.show', 'Cancel', $product->id, array('class' => 'btn')) }}

        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop