@extends('main')

@section('content')

    <h1>Products</h1>

    <p>{{ link_to_route('product.create', 'Add new product') }}</p>
    @if ($products->count())
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Clave</th>
                <th>Descripcion</th>
                <th>Precio</th>
                <th>Imagen</th>
            </tr>
            </thead>

            <tbody>

            @foreach ($products as $product)
                <tr>
                    <td>{{ $product['code'] }}</td>
                    <td>{{ $product['description'] }}</td>
                    <td>{{ $product['price'] }}</td>
                    <td>{{ $product['image'] }}</td>
                    <td>{{ link_to_route('product.edit', 'Edit', [$product['id']], array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['product.destroy', $product['id']]]) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
    @else
        There are no users
    @endif

@stop