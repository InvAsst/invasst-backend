@extends('main')

@section('content')

    <h1>Editar Cliente</h1>

    {{ Form::model($client, ['method' => 'PUT', 'route' => ['client.update', $client->id]]) }}

    <ul>

        <li>
            {{ Form::label('name', 'Nombre:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Apellido:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('email', 'Correo:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('phone', 'Telefono:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('business', 'Razon Social:') }}
            {{ Form::text('business') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn btn-info')) }}
            {{ link_to_route('client.show', 'Cancel', $client->id, array('class' => 'btn')) }}

        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop