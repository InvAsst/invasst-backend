@extends('main')

@section('content')

    <h1>Crear Clientes</h1>

    {{ Form::open(array('route' => 'client.store')) }}
    <ul>

        <li>
            {{ Form::label('name', 'Nombre:') }}
            {{ Form::text('name') }}
        </li>

        <li>
            {{ Form::label('last_name', 'Apellido:') }}
            {{ Form::text('last_name') }}
        </li>

        <li>
            {{ Form::label('email', 'Correo:') }}
            {{ Form::text('email') }}
        </li>

        <li>
            {{ Form::label('phone', 'Telefono:') }}
            {{ Form::text('phone') }}
        </li>

        <li>
            {{ Form::label('business', 'Razon Social:') }}
            {{ Form::text('business') }}
        </li>

        <li>
            {{ Form::submit('Submit', array('class' => 'btn')) }}
        </li>
    </ul>
    {{ Form::close() }}

    @if ($errors->any())
        <ul>
            {{ implode('', $errors->all('<li class="error">:message</li>')) }}
        </ul>
    @endif

@stop