@extends('main')

@section('content')

    <h1>Clientes</h1>

    <p>{{ link_to_route('client.create', 'Agregar nuevo cliente') }}</p>
    @if ($clients->count())
        <table class="table table-striped table-bordered">
            <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Correo</th>
                <th>Telefono</th>
                <th>Razon Social</th>
            </tr>
            </thead>

            <tbody>

            @foreach ($clients as $client)
                <tr>
                    <td>{{ $client['name'] }}</td>
                    <td>{{ $client['last_name'] }}</td>
                    <td>{{ $client['email'] }}</td>
                    <td>{{ $client['phone'] }}</td>
                    <td>{{ $client['business'] }}</td>
                    <td>{{ link_to_route('client.edit', 'Editar', [$client['id']], array('class' => 'btn btn-info')) }}</td>
                    <td>
                        {{ Form::open(['method' => 'DELETE', 'route' => ['client.destroy', $client['id']]]) }}
                        {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }}
                        {{ Form::close() }}
                    </td>
                </tr>
            @endforeach

            </tbody>

        </table>
    @else
        No hay clientes
    @endif

@stop