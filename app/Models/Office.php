<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Office extends Model
{
    protected $fillable = ['name', 'address', 'phone', 'rfc', 'logo', 'business'];

    static public function insert($data){

        $office = Office::create(array_filter($data, function($item){
            $office = new Office();
            return array_search($item, $office->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $office;
    }

    static public function select($id){
        if($id > 0)
            $office = Office::find($id);
        else
            $office = Office::all();

        return $office;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $office = new Office();
            return array_search($item, $office->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $office = Office::find($id);
        $office->update($data);

        return $office;
    }

    static public function remove($id){
        return Office::destroy($id);
    }
}
