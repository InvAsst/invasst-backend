<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{
    protected $fillable = ['stock', 'cost', 'description', 'code', 'image'];

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'product_component')->withPivot('amount');
    }

    static public function insert($data){

        $component = Component::create(array_filter($data, function($item){
            $component = new Component();
            return array_search($item, $component->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $component;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $components = Component::find($id);
        else
            $components = Component::all();

        if($extra){
                foreach ($components as &$component) {
                    $component['products'] = $component->products;
                }
        }

        return $components;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $component = new Component();
            return array_search($item, $component->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $component = Component::find($id);
        $component->update($data);

        return $component;
    }

    static public function remove($id){
        $component = Component::find($id);
        $component->products()->detach();
        return Component::destroy($id);
    }
}
