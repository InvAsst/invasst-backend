<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Quote extends Model
{
    protected $fillable = ['amount', 'description', 'user_id', 'transaction_id'];

    public function transaction(){
        return $this->belongsTo('App\Models\Transaction');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function products(){
        return $this->belongsToMany('App\Models\Product', 'quote_product')->withPivot('amount');
    }

    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'quote_package')->withPivot('amount');
    }

    public function calculateAmount(){
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->price * $product->pivot->amount;
        }

        return $total;
    }

    static public function insert($data, $productitems = [], $packageitems = []){

        $quote = Quote::create(array_filter($data, function($item){
            $quote = new Quote();
            return array_search($item, $quote->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        foreach ($productitems as $item) {
            $product = Product::find($item['id']);
            $quote->products()->save($product, ['amount' => $item['amount']]);
        }

        foreach ($packageitems as $item) {
            $package = Package::find($item['id']);
            $quote->packages()->save($package, ['amount' => $item['amount']]);
        }

        $quote->amount = $quote->calculateAmount();
        $quote->save();

        return $quote;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $quotes = Quote::find($id);
        else
            $quotes = Quote::all();

        if($extra){
            foreach ($quotes as &$quote) {
                $quote['products'] = $quote->products;
                $quote['pakcages'] = $quote->packages;
            }
        }

        return $quotes;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $quote = new Quote();
            return array_search($item, $quote->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $quote = Quote::find($id);
        $quote->update($data);

        return $quote;
    }

    static public function remove($id){
        $quote = Quote::find($id);
        $quote->products()->detach();
        $quote->packages()->detach();
        return Quote::destroy($id);
    }
}
