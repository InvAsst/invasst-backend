<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Component;

class Product extends Model
{
    // Columns
    protected $fillable = ['code', 'price', 'discount', 'description', 'image', 'category_id'];

    public function isFillableItem($item){
        $product = new Product();
        return array_search($item, $product->fillable) != false;
    }

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public function components(){
        return $this->belongsToMany('App\Models\Component', 'product_component')->withPivot('amount');
    }

    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'package_product')->withPivot('amount');
    }

    public function quotes(){
        return $this->belongsToMany('App\Models\Quote', 'quote_product')->withPivot('amount');
    }

    public function sales(){
        return $this->belongsToMany('App\Models\Sale', 'sale_product')->withPivot('amount');
    }
    
    static public function insert($data, $items = []){
        $product = Product::create(array_filter($data, function($item){
            $product = new Product();
            return array_search($item, $product->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        foreach ($items as $item) {
            $component = Component::find($item['id']);
            $product->components()->save($component, ['amount' => $item['amount']]);
        }

        return $product;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $products = Product::find($id);
        else
            $products = Product::all();

        if($extra){
            if(isset($products[0])) {
                foreach ($products as &$product) {
                    $product['components'] = $product->components;
                    $product['packages'] = $product->packages;
                    $product['category'] = $product->category;
                }
            }else{
                $products['components'] = $products->components;
                $products['packages'] = $products->packages;
                $products['category'] = $products->category;
            }
        }

        return $products;
    }

    static public function actualize($id, $data, $items){
        $data = array_filter($data, function($item){
            $product = new Product();
            return array_search($item, $product->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $product = Product::find($id);
        $product->update($data);

        $product->components()->detach();
        foreach ($items as $item) {
            $component = Component::find($item['id']);
            $product->components()->save($component, ['amount' => $item['amount']]);
        }
        
        return $product;
    }

    static public function remove($id){
        $product = Product::find($id);
        $product->components()->detach();
        $product->packages()->detach();
        return Product::destroy($id);
    }
    
    static public function totalPrice($products){
        $total = 0;
        foreach ($products as $product) {
            $total += $product['price'];
        }
        return $total;
    }
}
