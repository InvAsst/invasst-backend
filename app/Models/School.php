<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = ['name', 'phone', 'address'];

    public function clients(){
        return $this->hasMany('App\Models\Client');
    }

    static public function insert($data){

        $school = School::create(array_filter($data, function($item){
            $school = new School();
            return array_search($item, $school->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $school;
    }

    static public function select($id){
        if($id > 0)
            $school = School::find($id);
        else
            $school = School::all();

        return $school;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $school = new School();
            return array_search($item, $school->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $school = School::find($id);
        $school->update($data);

        return $school;
    }

    static public function remove($id){
        $school = School::find($id);
        $school->clients()->detach();
        return Client::destroy($id);
    }
}
