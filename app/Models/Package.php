<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['code', 'name', 'price', 'description', 'image'];

    public function products(){
        return $this->belongsToMany('App\Models\Product', 'package_product')->withPivot('amount');
    }

    public function quotes(){
        return $this->belongsToMany('App\Models\Quote', 'quote_package')->withPivot('amount');
    }

    public function sales(){
        return $this->belongsToMany('App\Models\Sale', 'sale_prackage')->withPivot('amount');
    }

    static public function insert($data, $items = []){

        $package = Package::create(array_filter($data, function($item){
            $package = new Package();
            return array_search($item, $package->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        foreach ($items as $item) {
            $product = Product::find($item['id']);
            $package->products()->save($product, ['amount' => $item['amount']]);
        }

        return $package;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $packages = Package::find($id);
        else
            $packages = Package::all();

        if($extra){
                foreach ($packages as &$package) {
                    $package['products'] = $package->products;
                }
        }

        return $packages;
    }

    static public function actualize($id, $data, $items){
        $data = array_filter($data, function($item){
            $package = new Package();
            return array_search($item, $package->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $package = Package::find($id);
        $package->update($data);

        $package->products()->detach();
        foreach ($items as $item) {
            $product = Product::find($item['id']);
            $package->products()->save($product, ['amount' => $item['amount']]);
        }

        return $package;
    }

    static public function remove($id){
        $package = Package::find($id);
        $package->products()->detach();
        return Package::destroy($id);
    }
}
