<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Transaction extends Model
{
    protected $fillable = ['client_id'];

    public function client(){
        return $this->belongsTo('App\Models\Client');
    }

    public function payments(){
        return $this->hasMany('App\Models\Payment');
    }

    public function quotes(){
        return $this->hasMany('App\Models\Quote');
    }

    public function sales(){
        return $this->hasMany('App\Models\Sale');
    }

    public function getCharged(){
        if(sizeof($this->sales) > 0){
            $operation = end($this->sales);
        } else {
            $operation = end($this->quotes);
        }

        return $operation[0]->amount;
    }

    public function getPayed(){
        $total = 0;
        foreach ($this->payments as $payment){
            $total += $payment->amount;
        }
        return $total;
    }

    public function getDebt(){
        return $this->getCharged() - $this->getPayed();
    }

    static public function insert($data){

        $transaction = Transaction::create(array_filter($data, function($item){
            $transaction = new Transaction();
            return array_search($item, $transaction->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $transaction;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $transactions = Transaction::find($id);
        else
            $transactions = Transaction::all();

        if($extra){
            if(isset($transactions[0])) {
                foreach ($transactions as &$transaction) {
                    $transaction['client'] = $transaction->client;
                    $transaction['payments'] = $transaction->payments;
                    $transaction['quotes'] = $transaction->quotes;
                    $transaction['sales'] = $transaction->sales;
                }
            } else {
                $transactions['client'] = $transactions->client;
                $transactions['payments'] = $transactions->payments;
                $transactions['quotes'] = $transactions->quotes;
                $transactions['sales'] = $transactions->sales;
            }
        }
        return $transactions;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $transaction = new Transaction();
            return array_search($item, $transaction->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $transaction = Transaction::find($id);
        $transaction->update($data);

        return $transaction;
    }

    static public function remove($id){
        $transaction = Transaction::find($id);
        return Transaction::destroy($id);
    }
}
