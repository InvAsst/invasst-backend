<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Client extends Model
{
    protected $fillable = ['name', 'last_name', 'business', 'phone', 'email', 'school_id'];

    public function school(){
        return $this->belongsTo('App\Models\School');
    }

    public function transactions(){
        return $this->hasMany('App\Models\Transaction');
    }
    
    static public function insert($data){

        $client = Client::create(array_filter($data, function($item){
            $client = new Client();
            return array_search($item, $client->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $client;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $clients = Client::find($id);
        else
            $clients = Client::all();

        if($extra){
            if(isset($clients[0])) {
                foreach ($clients as &$client) {
                    $client['school'] = $client->school;
                    $client['transactions'] = Transaction::select(
                        array_map(
                            function($transaction){return $transaction['id'];},
                            $client->transactions),
                        true);
                }
            } else {
                $transactionsid = array_map(
                    function($transaction){return $transaction['id'];},
                    $clients->transactions->toArray());

                $clients['school'] = $clients->school;
                $clients['transactions'] = Transaction::select(
                    $transactionsid,
                    true);
            }
        }

        return $clients;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $client = new Client();
            return array_search($item, $client->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $client = Client::find($id);
        $client->update($data);

        return $client;
    }

    static public function remove($id){
        return Client::destroy($id);
    }
}
