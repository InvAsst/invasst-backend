<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['description', 'code'];

    public function products(){
        return $this->hasMany('App\Models\Product');
    }

    static public function insert($data){

        $category = Category::create(array_filter($data, function($item){
            $category = new Category();
            return array_search($item, $category->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $category;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $categories = Category::find($id);
        else
            $categories = Category::all();

        if($extra){
                foreach ($categories as &$category) {
                    $category['products'] = $category->products;
                }
        }

        return $categories;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $category = new Category();
            return array_search($item, $category->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $category = Category::find($id);
        $category->update($data);

        return $category;
    }

    static public function remove($id){
        $category = Category::find($id);
        $category->products()->detach();
        return Category::destroy($id);
    }
}
