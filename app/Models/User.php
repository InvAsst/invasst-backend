<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'last_name', 'email', 'user', 'password', 'admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function payments(){
        return $this->hasMany('App\Models\Payment');
    }

    public function quotes(){
        return $this->hasMany('App\Models\Quote');
    }

    public function sales(){
        return $this->hasMany('App\Models\Sale');
    }
    
    static public function insert($data){

        $user = User::create(array_filter($data, function($item){
            $user = new User();
            return array_search($item, $user->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $user;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $users = USer::find($id);
        else
            $users = User::all();

        return $users;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $user = new User();
            return array_search($item, $user->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $user = User::find($id);
        $user->update($data);

        return $user;
    }

    static public function remove($id){
        return User::destroy($id);
    }
}
