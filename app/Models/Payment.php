<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = ['amount', 'description', 'user_id', 'transaction_id'];
    

    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    public function transaction(){
        return $this->belongsTo('App\Models\Transaction');
    }

    static public function insert($data){

        $payment = Payment::create(array_filter($data, function($item){
            $payment = new Payment();
            return array_search($item, $payment->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        return $payment;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $payments = Client::find($id);
        else
            $payments = Client::all();

        if($extra){
            foreach ($payments as &$payment) {
                $payment['transaction'] = $payment->transaction;
            }
        }

        return $payments;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $payment = new Payment();
            return array_search($item, $payment->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $payment = Payment::find($id);
        $payment->update($data);

        return $payment;
    }

    static public function remove($id){
        return Payment::destroy($id);
    }
}
