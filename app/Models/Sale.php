<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ['amount', 'description', 'user_id', 'transaction_id'];
    
    public function transaction(){
        return $this->belongsTo('App\Models\Transaction');
    }

    public function user(){
        return $this->belongsTo('App\Models\User');
    }
    
    public function products(){
        return $this->belongsToMany('App\Models\Product', 'sale_product')->withPivot('amount');
    }

    public function packages(){
        return $this->belongsToMany('App\Models\Package', 'sale_package')->withPivot('amount');
    }

    public function calculateAmount(){
        $total = 0;
        foreach ($this->products as $product) {
            $total += $product->price * $product->pivot->amount;
        }

        return $total;
    }

    static public function insert($data, $productitems = [], $packageitems = []){

        $sale = Sale::create(array_filter($data, function($item){
            $sale = new Sale();
            return array_search($item, $sale->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY));

        foreach ($productitems as $item) {
            $product = Product::find($item['id']);
            $sale->products()->save($product, ['amount' => $item['amount']]);
        }

        foreach ($packageitems as $item) {
            $package = Package::find($item['id']);
            $sale->packages()->save($package, ['amount' => $item['amount']]);
        }

        $sale->amount = $sale->calculateAmount();
        $sale->save();

        return $sale;
    }

    static public function select($id, $extra = false){
        if($id > 0)
            $sales = Quote::find($id);
        else
            $sales = Quote::all();

        if($extra){
            foreach ($sales as &$sale) {
                $sale['products'] = $sale->products;
                $sale['pakcages'] = $sale->packages;
            }
        }

        return $sales;
    }

    static public function actualize($id, $data){
        $data = array_filter($data, function($item){
            $sale = new Sale();
            return array_search($item, $sale->fillable) !== false;
        }, ARRAY_FILTER_USE_KEY);
        $sale = Sale::find($id);
        $sale->update($data);

        return $sale;
    }

    static public function remove($id){
        $sale = Sale::find($id);
        $sale->products()->detach();
        $sale->packages()->detach();
        return Sale::destroy($id);
    }
}
