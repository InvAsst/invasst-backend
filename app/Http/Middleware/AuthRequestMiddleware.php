<?php

namespace App\Http\Middleware;

use App\Http\Controllers\JsonResponse;
use Closure;
use Illuminate\Support\Facades\Auth;

class AuthRequestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!Auth::check())
            return JsonResponse::jsonError("Sesion Invalida");
        return $next($request);
    }
}
