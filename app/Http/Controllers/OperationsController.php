<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\Payment;
use App\Models\Quote;
use App\Models\Sale;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Log;
use App\Http\Requests;

class OperationsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth.request');
    }

    public function getQuote(Request $request, $id = -1){
        $quote = Quote::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($quote);
    }

    public function postQuote(Request $request, $transactionid = -1){
        if(sizeof($request->input('products', [])) <= 0)
            return JsonResponse::jsonError('Faltan productos');

        if($transactionid < 0){
            if($request->input("client_id", -1) < 0)
                return JsonResponse::jsonError('Necesita cliente');
            $transaction = Transaction::insert($request->all());
        } else {
            $transaction = Transaction::select($transactionid);
        }
        $data = array_merge($request->all(),
            ['transaction_id' => $transaction['id']],
            ['user_id' => $request->user()['id']]);

        $quote = Quote::insert($data, $request->input('products', []), $request->input('packages', []));
        return JsonResponse::jsonSuccess($quote);
    }

    public function getSale(Request $request, $id = -1){
        $sale = Sale::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($sale);
    }

    public function postSale(Request $request, $transactionid = -1){
        if(sizeof($request->input('products', [])) <= 0)
            return JsonResponse::jsonError('Faltan productos');

        if($transactionid < 0){
            if($request->input("client_id", -1) < 0)
                return JsonResponse::jsonError('Necesita cliente');
            $transaction = Transaction::insert($request->all());
        } else {
            $transaction = Transaction::select($transactionid);
        }
        $data = array_merge($request->all(),
            ['transaction_id' => $transaction['id']],
            ['user_id' => $request->user()['id']]);

        $sale = Sale::insert($data, $request->input('products', []), $request->input('packages', []));
        return JsonResponse::jsonSuccess($sale);
    }

    public function transactionsByClient(Request $request, $clientid = -1){
        $client = Client::select($clientid, true);
        return JsonResponse::jsonSuccess($client->transactions);
    }
    
    public function postPayment(Request $request, $transactionid = -1){
        if($transactionid < 0)
            return JsonResponse::jsonError("No hay transaccion asignada");
        $transaction = Transaction::select($transactionid);

        $data = array_merge($request->all(),
            ['transaction_id' => $transaction['id']],
            ['user_id' => $request->user()['id']]);

        $payment = Payment::insert($data);
        return JsonResponse::jsonSuccess($payment);
    }
}
