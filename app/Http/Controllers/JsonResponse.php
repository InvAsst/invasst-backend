<?php

/**
 * Created by IntelliJ IDEA.
 * User: edwin
 * Date: 1/21/2016
 * Time: 12:12 AM
 */

namespace App\Http\Controllers;

class JsonResponse
{
    static public function jsonSuccess($data = null) {
        $jsonResponse = ['status' => 'ok'];
        if(!is_null($data)) $jsonResponse['data'] = $data;
        return $jsonResponse;
    }

    static public function jsonError($msg) {
        return ['status' => 'error', 'message' => $msg];
    }
}