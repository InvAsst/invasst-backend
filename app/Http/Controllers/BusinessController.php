<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Models\School;
use Illuminate\Http\Request;

use App\Http\Requests;

class BusinessController extends Controller
{
    public function getIndex()
    {
        return JsonResponse::jsonSuccess("hola");
    }

    public function getClient(Request $request, $id = -1){
        $client = Client::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($client);
    }

    public function postClient(Request $request){
        $client = Client::insert($request->all());
        return JsonResponse::jsonSuccess($client);
    }

    public function putClient(Request $request, $id = -1){
        $client = Client::actualize($id, $request->all());
        return $client;
    }

    public function getSchool(Request $request, $id = -1){
        $school = School::select($id);
        return JsonResponse::jsonSuccess($school);
    }

    public function postSchool(Request $request){
        $school = School::insert($request->all());
        return JsonResponse::jsonSuccess($school);
    }

    public function putSchool(Request $request, $id = -1){
        $school = School::actualize($id, $request->all());
        return $school;
    }
}
