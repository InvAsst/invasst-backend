<?php

namespace App\Http\Controllers;

use App\Models\Client;
use App\Jobs\BatchInsertJob;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Files\ExcelFile;
use Symfony\Component\Process\Process;

class MiscellaneousController extends Controller
{
    public function postImport(Request $request, $table){
        $file = $request->file('import');

        $result = Excel::load($file->getRealPath())->get();
        $this->dispatch(new BatchInsertJob($result->toArray(), $table));
        for($i = 0; $i < 5; $i++) {
            $process = new Process('php ' . '/app/artisan queue:work --tries=1');
        //            $process->run(); // Sync
            $process->start(); // Async
        }
        return JsonResponse::jsonSuccess(['rows' => sizeof($result)]);
    }

    public function postUpload(Request $request){
        
    }
}
