<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Log;

class ViewsController extends Controller
{
    public function products(){
        $products = Product::select(-1);
        Log::info("PRODUCTS -> " . print_r($products, true));
        return view('products', ['products' => $products]);
    }
}
