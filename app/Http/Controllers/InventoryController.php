<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Component;
use App\Models\Package;
use App\Models\Product;
use Illuminate\Http\Request;

use App\Http\Requests;
use Log;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getIndex()
    {
        return JsonResponse::jsonSuccess("hola");
    }

    public function getComponent(Request $request, $id = -1) {
        $component = Component::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($component);
    }

    public function postComponent(Request $request) {
        $component = Component::insert($request->all());
        return JsonResponse::jsonSuccess($component);
    }

    public function putComponent(Request $request, $id = -1){
        $component = Component::actualize($id, $request->all());
        return $component;
    }

    public function deleteComponent(Request $request, $id){
        return JsonResponse::jsonSuccess(['rows' => Component::remove($id)]);
    }

    public function getProduct(Request $request, $id = -1) {
        $product = Product::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($product);
    }

    public function postProduct(Request $request) {
        $product = Product::insert($request->all(), $request->input('components', []));
        return JsonResponse::jsonSuccess($product);
    }

    public function putProduct(Request $request, $id = -1){
        $product = Product::actualize($id, $request->all(), $request->input('components', []));
        return $product;
    }

    public function deleteProduct(Request $request, $id){
        return JsonResponse::jsonSuccess(['rows' => Product::remove($id)]);
    }

    public function getPackage(Request $request, $id = -1) {
        $package = Package::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($package);
    }

    public function postPackage(Request $request) {
        $package = Package::insert($request->all(), $request->input('products', []));
        return JsonResponse::jsonSuccess($package);
    }

    public function putPackage(Request $request, $id = -1){
        $package = Package::actualize($id, $request->all(), $request->input('products', []));
        return $package;
    }

    public function deletePackage(Request $request, $id){
        return JsonResponse::jsonSuccess(['rows' => Package::remove($id)]);
    }

    public function getCategory(Request $request, $id = -1) {
        $categories = Category::select($id, $request->input('detailed', false));
        return JsonResponse::jsonSuccess($categories);
    }

    public function postCategory(Request $request) {
        $category = Category::insert($request->all());
        return JsonResponse::jsonSuccess($category);
    }

    public function putCategory(Request $request, $id = -1){
        $category = Category::actualize($id, $request->all());
        return $category;
    }

    public function deleteCategory(Request $request, $id){
        return JsonResponse::jsonSuccess(['rows' => Category::remove($id)]);
    }
}
