<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::resource('product', 'Views\ProductController');
Route::resource('component', 'Views\ComponentController');
Route::resource('package', 'Views\PackageController');
Route::resource('client', 'Views\ClientController');

Route::controller('/inv', 'InventoryController');
Route::controller('/bus', 'BusinessController');
Route::controller('/ops', 'OperationsController');

Route::controller('/misc', 'MiscellaneousController');
Route::auth();

Route::get('/home', 'HomeController@index');

Route::post('login', 'Auth\AuthController@login');
Route::get('transaction/client/{clientid}', 'OperationsController@transactionsByClient');
