<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use App\Client;
use App\Component;
use App\School;
use App\Category;
use App\Office;
use App\Package;
use App\Product;
use App\User;

class BatchInsertJob extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $data;
    protected $table;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data, $table)
    {
        $this->data = $data;
        $this->table = $table;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $rows = [];
        foreach ($this->data as $item){
            switch ($this->table){
                case 'client':
                    $rows[] = Client::insert($item);
                    break;
                case 'product':
                    $rows[] = Product::insert($item);
                    break;
                case 'component':
                    $rows[] = Component::insert($item);
                    break;
                case 'package':
                    $rows[] = Package::insert($item);
                    break;
                case 'category':
                    $rows[] = Category::insert($item);
                    break;
                case 'office':
                    $rows[] = Office::insert($item);
                    break;
                case 'school':
                    $rows[] = School::insert($item);
                    break;
                case 'user':
                    $rows[] = User::insert($item);
                    break;
            }

        }
    }
}

